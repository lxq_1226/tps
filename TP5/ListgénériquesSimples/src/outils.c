#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <assert.h>
#include "outils.h"

void printInteger ( int * i ) {
    printf( "La valeur est enti�re et vaut : %d\n", (*i) );
}

void rmInteger ( int * i ) {
    free( i );
}

bool intcmp ( int * i, int * j ) {
    return (*i) < (*j);
}

void printDouble ( double * d ) {
    printf( "La valeur est r�elle et vaut : %lf\n", (*d) );
}

void rmDouble ( double * d ) {
    free( d );
}

bool reelcmp ( double * u, double * v ) {
    return (*u) < (*v);
}

void freeInteger(int * i){
    assert(i);
    free(i);
}
