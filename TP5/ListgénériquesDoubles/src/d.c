#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>
#include "elmlist.h"
#include "list.h"
#include "d.h"

void write_list(struct lst_t * L,
                enum mode_t mode,
                void (*ptr_write)()) {
  FILE * fd;

 while(fd)
 {
    char full_file_name[40], file_name[20];
    printf("\t\tWrite list into %s file located in './data' directory\n",(mode == TEXT) ? "TXT" : "BIN");
    printf("\tFile name :");
    scanf("%s", file_name);
    sprintf(full_file_name, "./data/%s", file_name);
    fd = fopen(full_file_name, (mode == TEXT) ? "wt" : "wb");
  }


  for(struct lst_elm_t * iter = getHead(L); iter; iter = getSuc(iter) ) {
    (*ptr_write)(getDatum(iter), mode, fd);
  }

  fclose(fd);
}

struct lst_t * read_list(enum mode_t mode,
                        void * (*ptr_read)(),
                        bool (*ptr_cmp)()) {
  FILE * fd;

   while(fd)
   {
    char full_file_name[40], file_name[20];
    printf("\t\tRead list from %s file located in './data' directory\n",(mode == TEXT) ? "TXT" : "BIN");
    printf("\tFile name :");
    scanf("%s", file_name);
    sprintf(full_file_name, "./data/%s", file_name);
    fd = fopen(full_file_name, (mode == TEXT) ? "rt" : "rb");
  } 

  struct lst_t * L = new_lst();
  while(!feof(fd)) {
    void * data = (*ptr_read)(fd, mode);
    if(!data) continue;
    if (ptr_cmp) {
      insert_ordered(L, data, ptr_cmp);
    } else {
      queue(L, data);
    }
  }
  fclose(fd);
  return L;
}
