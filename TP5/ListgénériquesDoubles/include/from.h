#ifndef _FORM_
#define _FORM_
#define len_max 20
#include <stdio.h>
#include <stdbool.h>
#include "d.h"

struct form_t {
  char product[len_max];
  int stock;
  double pbt;
};

void del_form(struct form_t ** ptrF);

struct form_t * new_form();

struct form_t * read_form(FILE * fd, enum mode_t mode);

void write_form(struct form_t * F, enum mode_t mode, FILE * fd);

void view_form(struct form_t * F);

char * get_product(struct form_t * F);

int get_stock(struct form_t * F);

double get_price(struct form_t * F);

bool gt_form(struct form_t * F1, struct form_t * F2);

bool lt_form(struct form_t * F1, struct form_t * F2);

#endif // _FORM_
