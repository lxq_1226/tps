#ifndef _LST_T_H_
#define _LST_T_H_
struct lst_t {
	struct lst_elm_t* head;
	struct lst_elm_t* tail;
	int numelm;
};
/**
	* DÉCLARATION DES FONCTIONS ASSOCIÉES
	**/
	/* @brief Vérifier si la liste L est vide ou pas */
bool empty_lst(const struct lst_t* L);
/* @brief Construire une liste vide */
struct lst_t* new_lst();
/* @brief Ajouter en tête de la liste L la valeur v */
void cons(struct lst_t* L, int v);
/* @brief Visualiser les éléments de la liste L */
void print_lst(struct lst_t* L);
/* @brief Libèrer la mémoire occupée par la liste */
void del_lst(struct lst_t** ptrL);
#endif // !_LST_T_H_
